<?php
/**
 * Header theme template.
 *
 * @package iwp/iamaster
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta
			name="viewport"
			id="viewport"
			content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<?php wp_head(); ?>
	<!-- Meta Pixel Code -->
	<script>
		! function( f, b, e, v, n, t, s ) {
			if ( f.fbq ) return;
			n = f.fbq = function() {
				n.callMethod ?
					n.callMethod.apply( n, arguments ) : n.queue.push( arguments );
			};
			if ( ! f._fbq ) f._fbq = n;
			n.push = n;
			n.loaded = ! 0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement( e );
			t.async = ! 0;
			t.src = v;
			s = b.getElementsByTagName( e )[ 0 ];
			s.parentNode.insertBefore( t, s );
		}( window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js'
		);
		fbq( 'init', '578459757825757' );
		fbq( 'track', 'PageView' );
	</script>
	<noscript>
		<img
				height="1" width="1" style="display:none"
				src="https://www.facebook.com/tr?id=578459757825757&ev=PageView&noscript=1"/>
	</noscript>
	<!-- End Meta Pixel Code -->
</head>
<body <?php body_class(); ?>>
<div class="preloader">
	<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/img/logo.svg' ); ?>" alt="">
	<div class="preloader-line">
		<span></span>
	</div>
</div>
<header>
	<div class="vc_row wpb_row vc_row-fluid vc_row-o-content-middle vc_row-flex">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="wpb_column vc_column_container">
						<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
							<div
									class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12 vc_col-xs-12">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<div class="dfr">
											<?php the_custom_logo(); ?>
											<div class="burger-menu">
												<span></span><span></span><span></span>
											</div>
											<?php
											if ( has_nav_menu( 'top_bar_menu' ) ) {
												wp_nav_menu(
													[
														'theme_location' => 'top_bar_menu',
														'container'      => '',
														'menu_class'     => 'menu',
														'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
													]
												);
											}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
