/* global aiObject */

/**
 * @param aiObject.fromRedirectThanksPage
 * @param aiObject.eSputnikUrl
 */

jQuery( document ).ready( function( $ ) {
	loader();

	function loader( _success ) {
		let obj = document.querySelector( '.preloader' ),
			line = document.querySelector( '.preloader-line span' ),
			body = document.querySelector( 'body' );
		body.style.overflow = 'hidden';
		let w = 0,
			t = setInterval( function() {
				w = w + 1;
				line.style.left = w + '%';
				if ( w === 100 ) {
					body.style.overflowY = 'initial';
					obj.style.display = 'none';
					clearInterval( t );
					w = 0;
					if ( _success ) {
						return _success();
					}
				}
			}, 20 );
	}

	document.addEventListener( 'wpcf7mailsent', function( event ) {
		location.href = aiObject.fromRedirectThanksPage + event.detail.inputs[ 0 ].value;
	}, false );

	let menuButton = $( '.button.menu-item a' );

	if ( menuButton.length ) {
		menuButton.click( function( e ) {
			e.preventDefault();

			$( '.modal-form' ).modal( 'show' );
		} );
	}

	$( 'a[href*="#"]:not([href="#"]):not([data-toggle="tab"])' ).click( function() {
		if ( location.pathname.replace( /^\//, '' ) == this.pathname.replace( /^\//, '' ) && location.hostname == this.hostname ) {
			var target = $( this.hash );
			target = target.length ? target : $( '[name=' + this.hash.slice( 1 ) + ']' );
			if ( target.length ) {
				$( 'html,body' ).animate( {
					scrollTop: target.offset().top
				}, 1000 );
				return false;
			}
		}
	} );

} );
