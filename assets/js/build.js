'use strict';

jQuery( document ).ready( function( $ ) {
	// TABS START
	var tabsClick = function tabsClick() {
		$( '[data-toggle="tab"]' ).click( function( e ) {
			e.preventDefault();
			var hrefEl = $( this ).attr( 'href' );
			$( this ).parents( 'ul' ).children().removeClass( 'active' );
			$( hrefEl ).parents( '.tabs' ).children( 'div' ).removeClass( 'active' );
			$( this ).parent().addClass( 'active' );
			$( hrefEl ).addClass( 'active' );
		} );
	};

	tabsClick();

	// Data Title Before
	$( '.data-title' ).each( function() {
		var textEl = $( this ).text();
		$( this ).attr( 'data-title', textEl );
	} );

	// Data Text After
	$( '.data-text p' ).each( function() {
		var textAfter = $( this ).text();
		$( this ).parents( '.data-text' ).prev().attr( 'data-text', textAfter );
		$( this ).parents( '.data-text' ).remove();
	} );

	// FAQ Accordion
	$( '.faq-items h4' ).click( function() {
		$( this ).toggleClass( 'open' );
		$( this ).next().slideToggle();
	} );

} );