<?php
/**
 * Academy Ocean Cron.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme\Cron;

use AiMastery\Theme\Apis\Academyocean\AcademyoceanApi;
use AiMastery\Theme\Apis\Pipedrive\PipedriveApi;
use AiMastery\Theme\Main;
use GuzzleHttp\Exception\GuzzleException;

/**
 * AcademyOceanCron class file.
 */
class AcademyOceanCron {

	/**
	 * AcademyOceanCron construct
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Add hooks and actions.
	 *
	 * @return void
	 */
	public function init(): void {
		add_action( 'init', [ $this, 'add_schedule_cron' ] );

		add_filter( 'cron_schedules', [ $this, 'add_cron_interval' ] );
	}

	/**
	 * Add cron job.
	 *
	 * @return void
	 */
	public function add_schedule_cron(): void {
		if ( ! wp_next_scheduled( 'ai_academy_ocean_cron_update_user_info' ) ) {
			wp_schedule_event( time(), 'every_five_minutes', 'ai_academy_ocean_cron_update_user_info' );
		}

		add_action( 'ai_academy_ocean_cron_update_user_info', [ $this, 'user_info_update' ] );
	}

	/**
	 * Update user cron callback.
	 *
	 * @return void
	 * @throws GuzzleException
	 */
	public function user_info_update(): void {
		$users_email = self::get_all_sync_user_pipedrive();
		$academy_api = new AcademyoceanApi();
		$pipedrive   = new PipedriveApi();

		if ( ! empty( $users_email ) ) {
			foreach ( $users_email as $email ) {

				$user_info = $academy_api->get_user_by_email( null, $email );
				$user_fio  = [];

				if ( ! empty( $user_info ) ) {
					$user_fio['first_name'] = $user_info->first_name;
					$user_fio['last_name']  = $user_info->last_name;
				}

				$deal_info = $pipedrive->search_deal_by_email( $email );

				if ( ! $deal_info['user_status'] ) {
					$deal_info['user_status'] = 1;
					$user_name                = $user_fio['first_name'] . ' ' . $user_fio['last_name'];

					$pipedrive->update_user_deal( $deal_info['deal_id'], 2 );
					$pipedrive->update_fio_user( $deal_info['pip_user_id'], $user_name );
					self::update_user_status( $email );
				}
			}
		}
	}

	/**
	 * Get all user sync pipedrive.
	 *
	 * @return array
	 */
	public static function get_all_sync_user_pipedrive(): array {
		global $wpdb;

		$table_name = $wpdb->prefix . Main::AI_TABLE_NAME_PIPEDRIVE;

		$sql = "SELECT * FROM $table_name WHERE user_status = 0";

		$results     = $wpdb->get_results( $sql );
		$users_email = [];
		if ( ! empty( $results ) ) {

			foreach ( $results as $result ) {
				$users_email[] = $result->user_email;
			}

			return $users_email;
		}

		return [];
	}

	/**
	 * Update user status.
	 *
	 * @param string $email User email.
	 *
	 * @return void
	 */
	public static function update_user_status( string $email ): void {
		global $wpdb;

		$table_name = $wpdb->prefix . Main::AI_TABLE_NAME_PIPEDRIVE;

		$wpdb->update(
			$table_name,
			[
				'user_status' => 1,
			],
			[
				'user_email' => $email,
			],
			[ '%d' ],
			[ '%ss' ]
		);
	}

	/**
	 * Add schedules every five minutes.
	 *
	 * @param array $schedules Schedules.
	 *
	 * @return array
	 */
	public function add_cron_interval( array $schedules ): array {
		$schedules['every_five_minutes'] = [
			'interval' => 300,
			'display'  => __( 'Every Five Minutes', 'ai-mastery' ),
		];

		return $schedules;
	}
}
