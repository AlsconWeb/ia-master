<?php
/**
 * Products component wpBakery.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme\WpBakery\Components;

use AiMastery\Theme\Main;

/**
 * Products class file.
 */
class Products {
	/**
	 * Products construct.
	 */
	public function __construct() {
		add_shortcode( 'ai_product_learning', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ai_product_learning', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Product learning', 'ai-mastery' ),
			'description'             => esc_html__( 'Product learning', 'ai-mastery' ),
			'base'                    => 'ai_product_learning',
			'category'                => __( 'AI', 'ai-mastery' ),
			'show_settings_on_create' => false,
			'icon'                    => AI_THEME_ASSETS_URL . '/icons/graduation-cap-solid.svg',
			'params'                  => [
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::AI_DIR_PATH . '/WpBakery/Template/Products/template.php';

		return ob_get_clean();
	}
}
