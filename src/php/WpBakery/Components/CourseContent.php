<?php
/**
 * CourseContent component wpBakery.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme\WpBakery\Components;

use AiMastery\Theme\Main;

/**
 * CourseContent class file.
 */
class CourseContent {
	/**
	 * LearningProcess construct.
	 */
	public function __construct() {
		add_shortcode( 'ai_course_content', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ai_course_content', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Course content', 'ai-mastery' ),
			'description'             => esc_html__( 'Course content', 'ai-mastery' ),
			'base'                    => 'ai_course_content',
			'category'                => __( 'AI', 'ai-mastery' ),
			'show_settings_on_create' => false,
			'icon'                    => AI_THEME_ASSETS_URL . '/icons/file-lines-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Contents', 'ai-mastery' ),
					'param_name' => 'course_contents',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Title', 'ai-mastery' ),
							'param_name' => 'title',
						],
						[
							'type'       => 'textarea',
							'value'      => '',
							'heading'    => __( 'Description of the topic of the lecture', 'ai-mastery' ),
							'param_name' => 'topic_lecture',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Number of lectures', 'ai-mastery' ),
							'param_name' => 'number_lectures',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Lecture duration in minutes', 'ai-mastery' ),
							'param_name' => 'duration_lectures',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Number of tests', 'ai-mastery' ),
							'param_name' => 'number_test',
						],
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Number of practical tasks', 'ai-mastery' ),
							'param_name' => 'number_practical',
						],
						[
							'type'       => 'param_group',
							'value'      => '',
							'heading'    => __( 'Course points', 'ai-mastery' ),
							'param_name' => 'course_points',
							'params'     => [
								[
									'type'       => 'textfield',
									'value'      => '',
									'heading'    => __( 'Point', 'ai-mastery' ),
									'param_name' => 'point',
								],
							],
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::AI_DIR_PATH . '/WpBakery/Template/CourseContent/template.php';

		return ob_get_clean();
	}

}
