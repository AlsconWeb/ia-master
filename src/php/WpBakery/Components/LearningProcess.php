<?php
/**
 *  LearningProcess component wpBakery.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme\WpBakery\Components;

use AiMastery\Theme\Main;

/**
 * LearningProcess class file.
 */
class LearningProcess {

	/**
	 * Icons name Array
	 *
	 * @var array
	 */
	private array $icons;

	/**
	 * LearningProcess construct.
	 */
	public function __construct() {
		add_shortcode( 'ai_learn_process', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ai_learn_process', [ $this, 'map' ] );
		}

		$this->icons = [
			__( 'Desktop', 'ai-mastery' ) => 'icon-desktop',
			__( 'List', 'ai-mastery' )    => 'icon-list',
			__( 'Book', 'ai-mastery' )    => 'icon-book',
			__( 'Button', 'ai-mastery' )  => 'icon-button',
			__( 'Checked', 'ai-mastery' ) => 'icon-checked',
			__( 'Bag', 'ai-mastery' )     => 'icon-bag',
			__( 'Youtube', 'ai-mastery' ) => 'icon-youtube',
		];
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Learn Process', 'ai-mastery' ),
			'description'             => esc_html__( 'Learn Process', 'ai-mastery' ),
			'base'                    => 'ai_learn_process',
			'category'                => __( 'AI', 'ai-mastery' ),
			'show_settings_on_create' => false,
			'icon'                    => AI_THEME_ASSETS_URL . '/icons/chalkboard-user-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Processes', 'ai-mastery' ),
					'param_name' => 'learn_processes',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Title', 'ai-mastery' ),
							'param_name' => 'learn_title',
						],
						[
							'type'       => 'colorpicker',
							'value'      => '',
							'heading'    => __( 'Color block', 'ai-mastery' ),
							'param_name' => 'learn_color',
						],
						[
							'type'       => 'dropdown',
							'value'      => $this->icons,
							'heading'    => __( 'Icons name', 'ai-mastery' ),
							'param_name' => 'learn_icon',
						],
						[
							'type'       => 'textarea',
							'value'      => '',
							'heading'    => __( 'Description', 'ai-mastery' ),
							'param_name' => 'learn_description',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::AI_DIR_PATH . '/WpBakery/Template/LearningProcess/template.php';

		return ob_get_clean();
	}

}
