<?php
/**
 * FAQ template wpBakery.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme\WpBakery\Components;

use AiMastery\Theme\Main;

/**
 * FAQ class file.
 */
class FAQ {
	/**
	 * FAQ construct.
	 */
	public function __construct() {
		add_shortcode( 'ai_faq', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ai_faq', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'FAQ', 'ai-mastery' ),
			'description'             => esc_html__( 'FAQ', 'ai-mastery' ),
			'base'                    => 'ai_faq',
			'category'                => __( 'AI', 'ai-mastery' ),
			'show_settings_on_create' => false,
			'icon'                    => AI_THEME_ASSETS_URL . '/icons/list-check-solid.svg',
			'params'                  => [
				[
					'type'       => 'textfield',
					'value'      => '',
					'heading'    => __( 'Title', 'ai-mastery' ),
					'param_name' => 'title',
				],
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Question', 'ai-mastery' ),
					'param_name' => 'questions',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Question', 'ai-mastery' ),
							'param_name' => 'question',
						],
						[
							'type'       => 'textarea',
							'value'      => '',
							'heading'    => __( 'Answer', 'ai-mastery' ),
							'param_name' => 'answer',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::AI_DIR_PATH . '/WpBakery/Template/FAQ/template.php';

		return ob_get_clean();
	}
}
