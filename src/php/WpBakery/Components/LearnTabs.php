<?php

/**
 *  LearnTabs component wpBakery.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme\WpBakery\Components;

use AiMastery\Theme\Main;

/**
 * LearnTabs class file.
 */
class LearnTabs {
	/**
	 * LearnTabs construct.
	 */
	public function __construct() {
		add_shortcode( 'ai_learn_tabs', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ai_learn_tabs', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Learn Tabs', 'ai-mastery' ),
			'description'             => esc_html__( 'Learn Tabs', 'ai-mastery' ),
			'base'                    => 'ai_learn_tabs',
			'category'                => __( 'AI', 'ai-mastery' ),
			'show_settings_on_create' => false,
			'icon'                    => AI_THEME_ASSETS_URL . '/icons/diagram-next-solid.svg',
			'params'                  => [
				[
					'type'       => 'param_group',
					'value'      => '',
					'heading'    => __( 'Tabs', 'ai-mastery' ),
					'param_name' => 'learn_tab',
					'params'     => [
						[
							'type'       => 'textfield',
							'value'      => '',
							'heading'    => __( 'Title', 'ai-mastery' ),
							'param_name' => 'learn_title',
						],
						[
							'type'       => 'attach_image',
							'value'      => '',
							'heading'    => __( 'Image', 'ai-mastery' ),
							'param_name' => 'learn_image',
						],
						[
							'type'       => 'textarea',
							'value'      => '',
							'heading'    => __( 'Description', 'ai-mastery' ),
							'param_name' => 'learn_description',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::AI_DIR_PATH . '/WpBakery/Template/LearnTabs/template.php';

		return ob_get_clean();
	}

}
