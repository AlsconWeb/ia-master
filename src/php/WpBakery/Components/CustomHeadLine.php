<?php
/**
 * CustomHeadLine component wpBakery.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme\WpBakery\Components;

use AiMastery\Theme\Main;

/**
 * CustomHeadLine class.
 */
class CustomHeadLine {

	/**
	 * CustomHeadLine construct.
	 */
	public function __construct() {
		add_shortcode( 'ai_head_line', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ai_head_line', [ $this, 'map' ] );
		}
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Head line plus form', 'ai-mastery' ),
			'description'             => esc_html__( 'Head line plus form', 'ai-mastery' ),
			'base'                    => 'ai_head_line',
			'category'                => __( 'AI', 'ai-mastery' ),
			'show_settings_on_create' => false,
			'icon'                    => AI_THEME_ASSETS_URL . '/icons/heading-solid.svg',
			'params'                  => [
				[
					'type'       => 'textarea',
					'value'      => '',
					'heading'    => __( 'Head title', 'ai-mastery' ),
					'param_name' => 'head_title',
				],
				[
					'type'       => 'textarea',
					'value'      => '',
					'heading'    => __( 'Head description', 'ai-mastery' ),
					'param_name' => 'head_description',
				],
				[
					'type'       => 'textarea',
					'value'      => '',
					'heading'    => __( 'Form shot code', 'ai-mastery' ),
					'param_name' => 'form_short_code',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'Custom css', 'alevel' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design options', 'alevel' ),
				],
			],
		];
	}

	/**
	 * Output Short Code template
	 *
	 * @param mixed       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include Main::AI_DIR_PATH . '/WpBakery/Template/CustomHeadLine/template.php';

		return ob_get_clean();
	}
}
