<?php
/**
 *  LearnTabs template wpBakery.
 *
 * @package iwp/iamaster
 */

$learn_tabs = vc_param_group_parse_atts( $atts['learn_tab'] );
$css_class  = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>

<div class="dfr <?php echo esc_attr( $css_class ?? null ); ?>">
	<?php if ( ! empty( $learn_tabs ) ) { ?>
		<ul class="tab-nav">
			<?php
			foreach ( $learn_tabs as $key => $learn_tab ) {
				$image = wp_get_attachment_image( $learn_tab['learn_image'], 'full' );
				?>
				<li class="<?php echo esc_attr( 0 === $key ? 'active' : '' ); ?>">
					<?php
					if ( ! empty( $image ) ) {
						echo wp_kses_post( $image );
					}
					?>
					<h4><?php echo esc_html( $learn_tab['learn_title'] ); ?></h4>
					<a
						href="#<?php echo esc_attr( sanitize_title( $learn_tab['learn_title'] ) ); ?>"
						data-toggle="tab"></a>
				</li>
			<?php } ?>
		</ul>

		<div class="tabs">
			<?php foreach ( $learn_tabs as $key => $learn_tab_text ) { ?>
				<div
					class="tab-pane <?php echo esc_attr( 0 === $key ? 'active' : '' ); ?>"
					id="<?php echo esc_attr( sanitize_title( $learn_tab_text['learn_title'] ) ); ?>">
					<?php echo wp_kses_post( wpautop( $learn_tab_text['learn_description'] ) ); ?>
				</div>
			<?php } ?>
		</div>
		<?php
	}
	?>
</div>
