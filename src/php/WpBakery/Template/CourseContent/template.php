<?php
/**
 * CourseContent template wpBakery.
 *
 * @package iwp/iamaster
 */

$course_contents = vc_param_group_parse_atts( $atts['course_contents'] );
$css_class       = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

if ( ! empty( $course_contents ) ) {
	?>
	<div class="dfc <?php echo esc_attr( $css_class ?? null ); ?>">
		<div class="course-content">
			<?php foreach ( $course_contents as $course_content ) { ?>
				<div class="item">
					<div class="desc">
						<h3><?php echo esc_html( $course_content['title'] ); ?></h3>
						<?php echo wp_kses_post( wpautop( $course_content['topic_lecture'] ) ); ?>
						<ul>
							<li>
								<?php echo esc_html( $course_content['number_lectures'] ); ?>
								<span><?php echo esc_html( $course_content['duration_lectures'] ); ?></span>
							</li>
							<li>
								<?php echo esc_html( $course_content['number_test'] ); ?>
								<span><?php echo esc_html( $course_content['number_practical'] ); ?></span>
							</li>
						</ul>
					</div>
					<?php
					$course_point = vc_param_group_parse_atts( $course_content['course_points'] );

					if ( ! empty( $course_point ) ) {
						?>
						<ul>
							<?php foreach ( $course_point as $item ) { ?>
								<li><?php echo esc_html( $item['point'] ); ?></li>
							<?php } ?>
						</ul>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	</div>
	<?php
}
