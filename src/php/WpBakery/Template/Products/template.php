<?php
/**
 * Products template wpBakery.
 *
 * @package iwp/iamaster
 */

use AiMastery\Theme\Main;

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

$args = [
	'post_type'      => 'product',
	'posts_per_page' => - 1,
	'post_status'    => 'publish',
	'order'          => 'ASC',
	'orderby'        => 'ID',
];

$prod_query = new WP_Query( $args );

if ( $prod_query->have_posts() ) {
	?>
	<div class="tariff-items <?php echo esc_attr( $css_class ?? null ); ?>">
		<?php
		while ( $prod_query->have_posts() ) {
			$prod_query->the_post();

			$product    = wc_get_product( get_the_ID() );
			$price      = $product->get_regular_price();
			$sale_price = $product->get_sale_price();
			$attributes = $product->get_attributes();
			$url        = $product->add_to_cart_url();
			?>
			<div class="tariff-item">
				<h4><?php the_title(); ?></h4>
				<?php
				if ( 0 === (int) $price ) {
					?>
					<h3><?php esc_html_e( 'Free', 'ai-mastery' ); ?></h3>
					<?php
				} else {
					if ( ! empty( $product->get_date_on_sale_from() ) ) {

						printf(
							'<p class="timer icon-clock">%s</p>',
							esc_html( Main::get_countdown_sales( $product ) )
						);
					}
					?>
					<?php
					if ( $sale_price ) {
						printf(
							'<h3 data-procent="%d">%s<span>%s</span></h3>',
							esc_attr( ( ( $price - $sale_price ) / $price ) * 100 ),
							wp_kses_post( wc_price( $sale_price ) ),
							wp_kses_post( wc_price( $price ) )
						);
					} else {
						echo '<h3>' . wp_kses_post( wc_price( $price ) ) . '</h3>';
					}
				}

				the_content();

				if ( ! empty( $attributes ) ) {
					?>
					<ul>
						<?php
						foreach ( $attributes as $attribute ) {
							$name       = $attribute->get_name();
							$attr_value = explode( '|', $product->get_attribute( $name ) );
							foreach ( $attr_value as $value ) {
								?>
								<li class="icon-check"><?php echo esc_html( $value ); ?></li>
								<?php
							}
						}
						?>
					</ul>
				<?php } ?>
				<?php if ( 0 === (int) $price ) { ?>
					<a class="button transparent" href="#" data-toggle="modal" data-target=".modal-form">
						<?php esc_html_e( 'Try for free', 'ai-mastery' ); ?>
					</a>
				<?php } else { ?>
					<a class="button black" href="https://buy.stripe.com/28o2b1bBS7UtedObIL">
						<?php esc_html_e( 'Buy now', 'ai-mastery' ); ?>
					</a>
				<?php } ?>
			</div>
			<?php
		}
		wp_reset_postdata();
		?>
	</div>
	<?php
}
