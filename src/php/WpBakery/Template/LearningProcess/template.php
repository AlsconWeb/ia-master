<?php
/**
 * LearningProcess template wpBakery.
 *
 * @package iwp/iamaster
 */

$learn_processes = vc_param_group_parse_atts( $atts['learn_processes'] );
$css_class       = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

if ( ! empty( $learn_processes ) ) {
	?>
	<ul class="<?php echo esc_attr( $css_class ?? null ); ?>">
		<?php foreach ( $learn_processes as $process ) { ?>
			<li
				class="<?php echo esc_attr( $process['learn_icon'] ); ?>"
				style="color:<?php echo esc_attr( $process['learn_color'] ); ?>;">
				<h4><?php echo esc_html( $process['learn_title'] ); ?></h4>
				<?php echo wp_kses_post( wpautop( $process['learn_description'] ) ); ?>
			</li>
		<?php } ?>
	</ul>
	<?php
}
