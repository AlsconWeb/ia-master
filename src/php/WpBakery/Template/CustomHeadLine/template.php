<?php
/**
 * CustomHeadLine template wpBakery.
 *
 * @package iwp/iamaster
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>
<div class="dfc <?php echo esc_attr( $css_class ?? null ); ?>">
	<h2 class="title after"><?php echo wp_kses_post( $atts['head_title'] ); ?></h2>
	<div class="wpb_text_column wpb_content_element  data-text">
		<div class="wpb_wrapper">
			<?php echo wp_kses_post( wpautop( $atts['head_description'] ) ); ?>
		</div>
	</div>
	<?php
	echo do_shortcode( '[' . $atts['form_short_code'] . ']' );
	?>
</div>
