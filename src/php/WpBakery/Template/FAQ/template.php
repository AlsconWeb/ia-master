<?php
/**
 * FAQ template wpBakery.
 *
 * @package iwp/iamaster
 */

$css_class     = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$faq_questions = vc_param_group_parse_atts( $atts['questions'] );
?>

<div class="faq <?php echo esc_attr( $css_class ?? null ); ?>">
	<h2><?php echo esc_html( $atts['title'] ?? '' ); ?></h2>
	<div class="faq-items">
		<?php
		if ( ! empty( $faq_questions ) ) {
			foreach ( $faq_questions as $question ) {
				?>
				<div class="item">
					<h4><?php echo esc_html( $question['question'] ); ?></h4>
					<div class="hide">
						<?php echo wp_kses_post( wpautop( $question['answer'] ) ); ?>
					</div>
				</div>
				<?php
			}
		}
		?>
	</div>
</div>
