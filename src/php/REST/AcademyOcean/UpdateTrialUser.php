<?php
/**
 * Update trial user info.
 * AcademyOcean web hook academy.learner.sign_up.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme\REST\AcademyOcean;

use AiMastery\Theme\Apis\Pipedrive\PipedriveApi;
use GuzzleHttp\Exception\GuzzleException;
use WP_Error;
use WP_REST_Request;

/**
 *  UpdateTrialUser class file.
 */
class UpdateTrialUser {

	/**
	 * UpdateTrialUser construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
	}

	/**
	 * Update user and deal.
	 *
	 * @param WP_REST_Request $request WP Rest request.
	 *
	 * @return void|WP_Error
	 * @throws GuzzleException
	 */
	public function update_user_callback( WP_REST_Request $request ) {
		$user_email = ! empty( $request->get_params()['learner']['email'] ) ? filter_var( $request->get_params()['learner']['email'], FILTER_SANITIZE_EMAIL ) : null;

		if ( empty( $user_email ) ) {
			return new WP_Error(
				'empty_email',
				__( 'Email is empty or not valid', 'ai-mastery' ),
				[ 'status' => 422 ]
			);
		}
		
		$pipedrive = new PipedriveApi();

		$deal_info = $pipedrive->search_deal_by_email( $user_email );
		$pipedrive->add_user_in_db( $deal_info );
	}
}
