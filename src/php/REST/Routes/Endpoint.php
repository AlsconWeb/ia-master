<?php
/**
 * Add Endpoints in Rest API.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme\REST\Routes;

use AiMastery\Theme\Apis\Academyocean\AcademyoceanApi;
use AiMastery\Theme\REST\AcademyOcean\UpdateTrialUser;
use WP_REST_Server;

class Endpoint {

	/**
	 * REST API namespace.
	 */
	public const AI_REST_ROUTE_NAME_SPACE = 'ai-rest/v1';

	/**
	 * AcademyOcean web hook academy.learner.sign_up
	 * Update user endpoint.
	 */
	public const AI_USER_DATA_UPDATE = 'user-update';

	/**
	 * AcademyOcean get token.
	 */
	public const AI_GET_TOKEN_ACADEMY_OCEAN = 'ao-request-token';

	/**
	 * AcademyOcean set token.
	 */
	public const AI_SET_TOKEN_ACADEMY_OCEAN = 'ao-set-token';

	public const AI_GET_USER_FIO_ACADEMY_OCEAN_BY_EMAIL = 'ao-get-user-fio-by-email';

	/**
	 * Endpoint construct
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'rest_api_init', [ $this, 'register_rest_endpoint' ] );
	}

	/**
	 * Register endpoints.
	 *
	 * @return void
	 */
	public function register_rest_endpoint(): void {

		// update user.
		register_rest_route(
			self::AI_REST_ROUTE_NAME_SPACE,
			self::AI_USER_DATA_UPDATE,
			[
				'methods'  => WP_REST_Server::CREATABLE,
				'callback' => [ new UpdateTrialUser(), 'update_user_callback' ],
			]
		);

		// Auth in Academyocean.
		register_rest_route(
			self::AI_REST_ROUTE_NAME_SPACE,
			self::AI_GET_TOKEN_ACADEMY_OCEAN,
			[
				'methods'  => WP_REST_Server::READABLE,
				'callback' => [ new AcademyoceanApi(), 'send_request_token' ],
			]
		);

		register_rest_route(
			self::AI_REST_ROUTE_NAME_SPACE,
			self::AI_GET_USER_FIO_ACADEMY_OCEAN_BY_EMAIL,
			[
				'methods'  => WP_REST_Server::READABLE,
				'callback' => [ new AcademyoceanApi(), 'get_user_by_email' ],
				'args'     => [
					'email' => [
						'required' => true,
					],
				],
			]
		);
	}
}
