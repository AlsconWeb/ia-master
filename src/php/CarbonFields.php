<?php
/**
 * Carbone Fields init class.
 *
 * @package march/theme
 */

namespace AiMastery\Theme;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CarbonFields file.
 */
class CarbonFields {
	/**
	 * CarbonFields construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'after_setup_theme', [ $this, 'load_carbon_fields' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'register_theme_options' ] );
	}

	/**
	 * Load main class Carbon Field.
	 *
	 * @return void
	 */
	public function load_carbon_fields(): void {
		Carbon_Fields::boot();
	}

	/**
	 * Theme options.
	 *
	 * @return void
	 */
	public function register_theme_options(): void {
		$basic_options_container = Container::make( 'theme_options', __( 'Basic Options', 'ai-mastery' ) )
			->add_fields(
				[
					Field::make( 'header_scripts', 'ai_header_script', __( 'Header Script', 'ai-mastery' ) ),
					Field::make( 'footer_scripts', 'ai_footer_script', __( 'Footer Script', 'ai-mastery' ) ),
				]
			);

		Container::make( 'theme_options', __( 'Footer', 'ai-mastery' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'rich_text', 'ai_footer_text', __( 'Footer text', 'ai-mastery' ) ),
					Field::make( 'text', 'ai_footer_shot_code_form', __( 'Short code form', 'ai-mastery' ) ),
				]
			);
		Container::make( 'theme_options', __( 'Modal', 'ai-mastery' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'text', 'ai_modal_title', __( 'Modal title', 'ai-mastery' ) ),
					Field::make( 'rich_text', 'ai_modal_text', __( 'Modal text', 'ai-mastery' ) ),
				]
			);

		Container::make( 'theme_options', __( 'Apis Settings', 'ai-mastery' ) )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'text', 'ai_api_key_esputnik', __( 'Api key eSputnik', 'ai-mastery' ) )
						->set_attribute( 'type', 'password' ),
					Field::make( 'text', 'ai_api_key_pipedrive', __( 'Api key Pipedrive', 'ai-mastery' ) )
						->set_attribute( 'type', 'password' ),
					Field::make( 'text', 'ai_api_client_id', __( 'Academyocean client id', 'ai-mastery' ) )
						->set_attribute( 'type', 'password' ),
					Field::make( 'text', 'ai_api_client_secret', __( 'Academyocean client secret', 'ai-mastery' ) )
						->set_attribute( 'type', 'password' ),
				]
			);
	}
}
