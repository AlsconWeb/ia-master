<?php
/**
 * Api Pipedrive crm.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme\Apis\Pipedrive;

use AiMastery\Theme\Main;
use GuzzleHttp\Client;
use Pipedrive\APIException;
use Pipedrive\APIHelper;
use Pipedrive\Client as PipedriveClient;
use Pipedrive\Configuration;
use Pipedrive\Controllers\DealsController;
use Pipedrive\Controllers\PersonsController;

/**
 * PipedriveApi class file.
 */
class PipedriveApi {

	private const AI_NEW_LEAD = '/leads';
	/**
	 * Http client.
	 *
	 * @var Client
	 */
	private Client $http_client;
	/**
	 * Pipedrive Client class.
	 *
	 * @var PipedriveClient
	 */
	private PipedriveClient $new_client;

	/**
	 * PipedriveApi construct.
	 */
	public function __construct() {
		$this->new_client  = new PipedriveClient( null, null, null, $this->get_api_key() );
		$this->http_client = new Client();
	}

	/**
	 * Get pipedrive api key.
	 *
	 * @return string
	 */
	private function get_api_key(): string {

		return carbon_get_theme_option( 'ai_api_key_pipedrive' ) ?? '';
	}

	/**
	 * Crete lead and deal.
	 *
	 * @param object $data Preson data.
	 *
	 * @return void
	 * @throws APIException
	 */
	public function create_lead( object $data ): void {
		$new_user = new PersonsController();
		APIHelper::cleanUrl( Configuration::getBaseUri() . self::AI_NEW_LEAD );

		$org_id = 0;

		if ( ! empty( $data ) ) {
			$person_data = [
				'name'                                     => $data->name,
				'phone'                                    => $data->phone,
				'email'                                    => $data->email,
				'c5901b0dada8f88a5c697d97710af1c4bfd015bf' => $data->state ?? '',
				'482b4a6b28cad44cdef5c90f5c86fb8ce6fe4065' => $data->line1,
				'b135ad154f4183539dcef5df227f051229dc72d3' => $data->line2,
			];

			$response = $new_user->addAPerson(
				$person_data
			);

			if ( $response->success ) {
				$id = $response->data->id;

				$deal = new DealsController();

				$res = $deal->addADeal(
					[
						'title'                                    => 'Deal of ai-mastery',
						'person_id'                                => $id,
						'value'                                    => (string) ( $data->amount_total / 100 ),
						'b68b349537a05ecc6bf9404384ae6a74f0cfd0c9' => $data->currency,
						'b1e2b29811fd9d8daf10fedb18d9ca38d9e1b664' => $data->city,
						'6a2593ce670d801f5965a64e4f61360e5d191f9f' => $data->country,
						'4f3eedf6c57388c99ff34a7776bd6f85e08826fd' => $data->postal_code,
					]
				);
			}
		}
	}

	/**
	 * Create trial lead.
	 *
	 * @param object $data Customer data.
	 *
	 * @return void
	 * @throws APIException
	 */
	public function create_trial_lead( object $data ): void {
		$new_user = new PersonsController();
		APIHelper::cleanUrl( Configuration::getBaseUri() . self::AI_NEW_LEAD );

		if ( ! empty( $data ) ) {
			$person_data = [
				'name'  => 'New trial user',
				'email' => $data->email,
			];

			$response = $new_user->addAPerson(
				$person_data
			);

			if ( $response->success ) {
				$id = $response->data->id;

				$deal = new DealsController();

				$res = $deal->addADeal(
					[
						'title'     => 'New trial user',
						'person_id' => $id,
					]
				);

				self::add_user_in_db(
					[
						'pip_user_id' => $id,
						'user_email'  => $data->email,
						'id_stripe'   => null,
						'deal_id'     => $res->data->id,
						'user_status' => 0,
						'status_sync' => 0,
					]
				);
			}
		}
	}

	/**
	 * Add user deal in db.
	 *
	 * @param array $user_data User data.
	 *
	 * @return bool
	 */
	public function add_user_in_db( array $user_data ): bool {
		global $wpdb;

		$table_name = $wpdb->prefix . Main::AI_TABLE_NAME_PIPEDRIVE;

		$res = $wpdb->insert(
			$table_name,
			$user_data,
			[
				'%d',
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
			]
		);

		if ( $res ) {
			return true;
		}

		return false;
	}

	/**
	 * Search deal by user email.
	 *
	 * @param string $user_email User email.
	 *
	 * @return array|false
	 */
	public function search_deal_by_email( string $user_email ) {
		if ( empty( $user_email ) || ! filter_var( $user_email, FILTER_VALIDATE_EMAIL ) ) {
			return false;
		}

		global $wpdb;

		$table_name = $wpdb->prefix . Main::AI_TABLE_NAME_PIPEDRIVE;

		$sql = "SELECT * FROM $table_name WHERE user_email = %s";

		$result = $wpdb->get_results(
			$wpdb->prepare( $sql, $user_email )
		);

		if ( ! empty( $result ) ) {
			return [
				'pip_user_id' => $result[0]->pip_user_id,
				'user_email'  => $user_email,
				'id_stripe'   => $result[0]->id_stripe,
				'deal_id'     => $result[0]->deal_id,
				'user_status' => $result[0]->user_status,
				'status_sync' => $result[0]->stsus_sync,
			];
		}

		try {
			$user_deal = new DealsController();

			$res = $user_deal->searchDeals(
				[
					'term'           => $user_email,
					'exactMatch'     => true,
					'fields'         => [ 'title' ],
					'include_fields' => true,
				]
			);

			if ( $res->success ) {
				$result_array = [];
				foreach ( $res->data->items as $item ) {
					$result_array = [
						'pip_user_id' => $item->item->person->id,
						'user_email'  => $item->item->title,
						'deal_id'     => $item->item->id,
						'id_stripe'   => null,
						'user_status' => (bool) $item->item->customFields[0],
						'status_sync' => 0,
					];
				}

				return $result_array;
			}
		} catch ( APIException $e ) {
			return [
				'success' => false,
				'error'   => $e->getMessage(),
			];
		}

		return [];
	}

	/**
	 * Update deal user.
	 *
	 * @param int $deal_id  Deal ID.
	 * @param int $stage_id Stage ID.
	 *
	 * @return array|true[]
	 */
	public function update_user_deal( int $deal_id, int $stage_id ) {
		$deal = new DealsController();

		try {
			$deal->updateADeal(
				[
					'id'       => $deal_id,
					'stage_id' => $stage_id,
				]
			);

			return [ 'success' => true ];

		} catch ( APIException $e ) {
			return [
				'success' => false,
				'error'   => $e->getMessage(),
			];
		}
	}

	/**
	 * Update user fio.
	 *
	 * @param int    $user_id User id.
	 * @param string $fio     FIO user.
	 *
	 * @return array|true[]
	 */
	public function update_fio_user( int $user_id, string $fio ) {
		$person = new PersonsController();

		try {
			$person->updateAPerson(
				[
					'id'   => $user_id,
					'name' => $fio,
				]
			);

			return [ 'success' => true ];
		} catch ( APIException $e ) {
			return [
				'success' => false,
				'error'   => $e->getMessage(),
			];
		}
	}
}
