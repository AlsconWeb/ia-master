<?php
/**
 * eSputnik api.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme\Apis\ESputnik;

use GuzzleHttp\Client;

/**
 * ESputnikApi class file.
 */
class ESputnikApi {

	/**
	 * Description: eSputnik event url.
	 *
	 * @var string
	 */
	public const AI_EVENT_PONT_URL = 'https://esputnik.com/api/v2/event';

	/**
	 * Description: eSputnik subscribe url.
	 */
	public const  E_SPUTNIK_SUBSCRIBE_URL = 'https://esputnik.com/api/v1/contact/subscribe';

	/**
	 * Description: eSputnik api key.
	 *
	 * @var string
	 */
	private string $api_key;

	/**
	 * Customers  data.
	 *
	 * @var array Data from stripe.
	 */
	private array $data;

	/**
	 * GuzzleHttp client.
	 *
	 * @var Client
	 */
	private $http_client;

	/**
	 * ESputnikApi construct.
	 *
	 * @param array $data Customer data from stripe.
	 */
	public function __construct( array $data ) {
		$this->api_key = carbon_get_theme_option( 'ai_api_key_esputnik' ) ?? '';
		$this->data    = $data;

		$this->http_client = new Client();
	}

	public function send_event( string $endpoin_name ) {

		$response = $this->http_client->request(
			'POST',
			$this->get_endpoint_url( 'event' ),
			[
				'headers' => [
					'accept'        => 'application/json; charset=UTF-8',
					'content-type'  => 'application/json',
					'authorization' => base64_encode( 'aimasteryacademy' + ':' + $this->api_key ),
				],
			]
		);
	}

	/**
	 * Get endpoint url.
	 *
	 * @param string $endpoint_name eSpitnik endpoint name.
	 *
	 * @return string
	 */
	private function get_endpoint_url( string $endpoint_name ): string {
		switch ( $endpoint_name ) {
			case 'event':
				$endpoint_url = self::AI_EVENT_PONT_URL;
				break;
			case 'subscribe':
				$endpoint_url = self::E_SPUTNIK_SUBSCRIBE_URL;
				break;
			default:
				$endpoint_url = '';
		}

		return $endpoint_url;
	}

	private function get_cusomer_data() {
		$data = [
			'city'         => 'Warshaw',
			'country'      => 'PL',
			'line1'        => 'Jasnodworska 7 apart. 5',
			'line2'        => '',
			'postal_code'  => '01-074',
			'state'        => '',
			'email'        => 'glazoknet@gmail.com',
			'name'         => 'Oleksandr Lavyhin',
			'phone'        => '+48737414783',
			'amount_total' => '9922',
			'currency'     => 'usd',
		];
	}
}
