<?php
/**
 * Academyocean Apis.
 *
 * @package iwp/iamaster
 */


namespace AiMastery\Theme\Apis\Academyocean;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * AcademyoceanApi class file.
 */
class AcademyoceanApi {

	/**
	 * Get token url.
	 */
	private const AI_AUTH_GET_TOKEN_URL = 'https://app.academyocean.com/oauth/token';

	/**
	 * Base url api Academy ocean.
	 */
	private const AI_ACADEMY_BASE_URL = 'https://app.academyocean.com/api/v1/';

	/**
	 * Http client GuzzleHttp.
	 *
	 * @var Client
	 */
	public $client;

	/**
	 * Client id.
	 *
	 * @var string
	 */
	private string $client_id;

	/**
	 * Client secret.
	 *
	 * @var string
	 */
	private string $client_secret;

	/**
	 * Token type.
	 *
	 * @var string|false
	 */
	private string $token_type;

	/**
	 * Token.
	 *
	 * @var string|false
	 */
	private string $token;

	/**
	 * AcademyoceanApi construct.
	 */
	public function __construct() {
		$this->client = new Client();

		$this->token_type = get_option( 'academyocean_token_type', false );
		$this->token      = get_option( 'academyocean_token', false );

		if ( ! $this->token || ! $this->token ) {
			$this->send_request_token();
		}
	}

	/**
	 * Send request get token.
	 *
	 * @return mixed|void
	 * @throws GuzzleException
	 */
	public function send_request_token() {
		$this->client_id     = carbon_get_theme_option( 'ai_api_client_id' ) ?? '';
		$this->client_secret = carbon_get_theme_option( 'ai_api_client_secret' ) ?? '';

		try {
			$response = $this->client->post(
				self::AI_AUTH_GET_TOKEN_URL,
				[
					'form_params' => [
						'grant_type'    => 'client_credentials',
						'client_id'     => $this->client_id,
						'client_secret' => $this->client_secret,
						'scope'         => '',
					],
				]
			);

			$auth = json_decode( (string) $response->getBody() );

			if ( empty( $auth->error ) ) {
				add_option( 'academyocean_token_type', $auth->token_type );
				add_option( 'academyocean_token', $auth->access_token );
			}
		} catch ( ClientException $e ) {
			if ( $e->hasResponse() ) {
				$response      = $e->getResponse();
				$json_response = json_decode( $response->getBody(), true );

				return $json_response;
			}

			throw $e;
		}
	}

	/**
	 * Get user info by email.
	 *
	 * @param $request
	 * @param $email
	 *
	 * @return mixed|object
	 * @throws GuzzleException
	 */
	public function get_user_by_email( $request = null, $email = null ) {

		if ( ! empty( $request ) ) {
			$user_email = $request->get_param( 'email' );
		} else {
			$user_email = $email;
		}

		try {
			$response = $this->client->post(
				self::AI_ACADEMY_BASE_URL,
				[
					'headers'     => [
						'Authorization' => $this->token_type . ' ' . $this->token,
					],
					'form_params' => [
						'action'  => 'learners',
						'options' => [
							'email'        => $user_email,
							'academyocean' => true,
							'with_teams'   => true,
						],
					],
				]
			);

			$learners = json_decode( (string) $response->getBody() );

			if ( ! empty( $learners ) ) {
				return $learners[0];
			}

			return (object) [];
		} catch ( ClientException $e ) {
			if ( $e->hasResponse() ) {
				$response      = $e->getResponse();
				$json_response = json_decode( $response->getBody(), true );

				return $json_response;
			}

			throw $e;
		}
	}
}
