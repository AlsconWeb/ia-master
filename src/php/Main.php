<?php
/**
 * Main theme class.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme;

use AiMastery\Theme\Apis\ESputnik\ESputnikApi;
use AiMastery\Theme\Apis\Pipedrive\PipedriveApi;
use AiMastery\Theme\Cron\AcademyOceanCron;
use AiMastery\Theme\REST\Routes\Endpoint;
use AiMastery\Theme\WpBakery\Components\CourseContent;
use AiMastery\Theme\WpBakery\Components\CustomHeadLine;
use AiMastery\Theme\WpBakery\Components\FAQ;
use AiMastery\Theme\WpBakery\Components\LearningProcess;
use AiMastery\Theme\WpBakery\Components\LearnTabs;
use AiMastery\Theme\WpBakery\Components\Products;
use Pipedrive\APIException;
use WPCF7_ContactForm;

/**
 * Main class file.
 */
class Main {

	/**
	 * Theme version.
	 */
	public const AI_THEME_VERSION = '1.1.1';

	/**
	 * Dir path.
	 */
	public const AI_DIR_PATH = __DIR__;

	/**
	 * LMS E Sputnik url.
	 */
	public const E_SPUTNIK_SUBSCRIBE_URL = 'https://esputnik.com/api/v1/contact/subscribe';

	/**
	 * Table name stripe.
	 */
	public const AI_TABLE_NAME_STRIPE = 'ai_stripe_requests';

	/**
	 * Table name pipedrive.
	 */
	public const AI_TABLE_NAME_PIPEDRIVE = 'ai_pipedrive_users';

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();

		new CarbonFields();
		new Endpoint();
		new AcademyOceanCron();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {

		// actions.
		add_action( 'after_setup_theme', [ $this, 'add_theme_support' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
		add_action( 'widgets_init', [ $this, 'register_widget_zone' ] );
		add_action( 'vc_before_init', [ $this, 'add_bakery_components' ] );
		add_action( 'init', [ $this, 'add_stripe_table' ] );
		add_action( 'init', [ $this, 'add_pipedrive_table' ] );
		add_action( 'wpcf7_before_send_mail', [ $this, 'create_new_trial_user' ] );

		// filters.
		add_filter( 'mime_types', [ $this, 'add_support_mimes' ] );
		add_filter( 'get_custom_logo', [ $this, 'output_logo' ] );
		add_filter( 'woocommerce_add_to_cart_redirect', [ $this, 'custom_add_to_cart_redirect' ] );
		add_filter( 'wc_add_to_cart_message', [ $this, 'remove_wc_add_to_cart_message' ], 10, 2 );
		add_filter( 'woocommerce_countries', [ $this, 'exclude_countries_from_checkout' ] );
	}

	/**
	 * Get countdown sales.
	 *
	 * @param object $product
	 *
	 * @return string
	 */
	public static function get_countdown_sales( object $product ): string {
		$current_timestamp = current_time( 'timestamp' );
		$end_timestamp     = strtotime( $product->get_date_on_sale_to()->date( 'Y-m-d' ) );

		if ( $end_timestamp > $current_timestamp ) {
			$remaining_time = $end_timestamp - $current_timestamp;

			if ( $remaining_time > 24 * 60 * 60 ) {
				$days_left = ceil( $remaining_time / ( 24 * 60 * 60 ) );

				return $days_left . __( ' days left at this price', 'ai-mastery' );
			} else {
				$hours_left = ceil( $remaining_time / ( 60 * 60 ) );

				return $hours_left . __( ' hours left', 'ai-mastery' );
			}
		}

		return '';
	}

	/**
	 * Theme support.
	 *
	 * @return void
	 */
	public function add_theme_support(): void {

		// add custom logo.
		add_theme_support( 'custom-logo' );

		// add menu support.
		add_theme_support( 'menus' );

		register_nav_menus(
			[
				'top_bar_menu' => __( 'Top bar menu', 'ai-mastery' ),
				'footer_menu'  => __( 'Footer menu', 'ai-mastery' ),
			]
		);
	}

	/**
	 * Add scripts and style.
	 *
	 * @return void
	 */
	public function add_scripts(): void {

		$url = get_stylesheet_directory_uri();
		$min = '.min';

		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
			$min = '';
		}

		wp_enqueue_script(
			'build',
			$url . '/assets/js/build' . $min . '.js',
			[
				'jquery',

			],
			self::AI_THEME_VERSION,
			true
		);

		wp_enqueue_script(
			'main',
			$url . '/assets/js/main' . $min . '.js',
			[
				'jquery',
			],
			self::AI_THEME_VERSION,
			true
		);

		wp_enqueue_script(
			'modal',
			$url . '/assets/js/modal.js',
			[
				'jquery',
				'main',
			],
			self::AI_THEME_VERSION,
			true
		);

		wp_localize_script(
			'main',
			'aiObject',
			[
//				'fromRedirectThanksPage' => 'https://lms.ai-mastery.academy/auth/sign/in#',
				'fromRedirectThanksPage' => 'https://test-ai-mastery-academy.academyocean.com/auth/sign/in#',
				'eSputnikUrl'            => ESputnikApi::E_SPUTNIK_SUBSCRIBE_URL,
			]
		);

		wp_enqueue_script( 'html5shiv', '//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', [], '3.7.0', false );
		wp_enqueue_script( 'respond', '//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', [], '1.4.2', false );
		wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
		wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

		wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css2?family=Noto+Sans&amp;display=swap', '', self::AI_THEME_VERSION );
		wp_enqueue_style( 'modal', $url . '/assets/css/modal' . $min . '.css', '', self::AI_THEME_VERSION );
		wp_enqueue_style( 'main', $url . '/assets/css/main' . $min . '.css', '', self::AI_THEME_VERSION );
		wp_enqueue_style( 'style', $url . '/style.css', '', self::AI_THEME_VERSION );

		if ( is_checkout() ) {
			wp_enqueue_style( 'bakery', $url . '/assets/css/wp_bakery' . $min . '.css', '', self::AI_THEME_VERSION );
		}
	}

	/**
	 * Add SVG and Webp formats to upload.
	 *
	 * @param array $mimes Mimes type.
	 *
	 * @return array
	 */
	public function add_support_mimes( array $mimes ): array {

		$mimes['webp'] = 'image/webp';
		$mimes['svg']  = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Change output custom logo.
	 *
	 * @param string $html HTML custom logo.
	 *
	 * @return string
	 */
	public function output_logo( string $html ): string {

		$home  = esc_url( get_bloginfo( 'url' ) );
		$class = 'logo-img';
		if ( has_custom_logo() ) {
			$logo    = wp_get_attachment_image(
				get_theme_mod( 'custom_logo' ),
				'full',
				false,
				[
					'class'    => 'logo',
					'itemprop' => 'logo',
				]
			);
			$content = $logo;

			$html = sprintf(
				'<a href="%s" class="%s" rel="home" itemprop="url">%s</a>',
				$home,
				$class,
				$content
			);

		}

		return $html;
	}

	/**
	 * Register widget zone.
	 *
	 * @return void
	 */
	public function register_widget_zone(): void {
		register_sidebar(
			[
				'name'          => __( 'Footer one', 'ai-mastery' ),
				'id'            => 'footer-one',
				'description'   => __( 'Add widgets in footer menu', 'ai-mastery' ),
				'before_widget' => '<div class="wpb_wrapper">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4>',
				'after_title'   => '</h4>',
			]
		);

		register_sidebar(
			[
				'name'          => __( 'Footer two', 'ai-mastery' ),
				'id'            => 'footer-two',
				'description'   => 'Add widgets in footer contact',
				'before_widget' => '<div class="wpb_wrapper">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4>',
				'after_title'   => '</h4>',
			]
		);
	}

	/**
	 * Add WPBakery components.
	 *
	 * @return void
	 */
	public function add_bakery_components(): void {
		new LearnTabs();
		new LearningProcess();
		new CourseContent();
		new CustomHeadLine();
		new Products();
		new FAQ();
	}

	/**
	 * Add to cart redirect to check out.
	 *
	 * @return string
	 */
	public function custom_add_to_cart_redirect(): string {
		return wc_get_checkout_url();
	}

	/**
	 * Remove message add to cart.
	 *
	 * @param string $message    Message.
	 * @param int    $product_id Product ID.
	 *
	 * @return string
	 */
	public function remove_wc_add_to_cart_message( string $message, int $product_id ): string {

		return '';
	}

	/**
	 * Exclude countries from checkout.
	 *
	 * @param $countries
	 *
	 * @return mixed
	 */
	public function exclude_countries_from_checkout( $countries ): mixed {
		$excluded_countries = [ 'US', 'CA', 'GB' ];

		foreach ( $countries as $country_code => $country_name ) {
			if ( ! in_array( $country_code, $excluded_countries ) ) {
				unset( $countries[ $country_code ] );
			}
		}

		return $countries;
	}

	/**
	 * Create stripe table.
	 *
	 * @return void
	 */
	public function add_stripe_table(): void {

		$created = get_option( 'stripe_table_created', false );

		if ( ! $created ) {
			global $wpdb;

			$table_name = $wpdb->prefix . self::AI_TABLE_NAME_STRIPE;

			$sql = "CREATE TABLE $table_name (
				`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
				`stripe_event_id` varchar(255) DEFAULT NULL,
				`amount_total` int(11) DEFAULT NULL,
				`currency` varchar(50) DEFAULT NULL,
				`customer_details` varchar(255) DEFAULT NULL,
				`payment_status` varchar(50) DEFAULT NULL,
				`status_check` tinyint(1) NOT NULL DEFAULT '0',
				`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY (`id`)
				);";

			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			dbDelta( $sql );

			add_option( 'stripe_table_created', true );
		}
	}

	/**
	 * Create Pipedrive table.
	 *
	 * @return void
	 */
	public function add_pipedrive_table(): void {
		$created = get_option( 'pipedrive_table_created', false );

		if ( ! $created ) {
			global $wpdb;

			$table_name = $wpdb->prefix . self::AI_TABLE_NAME_PIPEDRIVE;

			$sql = "CREATE TABLE $table_name (
    				`id` BIGINT NOT NULL AUTO_INCREMENT , 
    				`pip_user_id` BIGINT NULL DEFAULT NULL , 
    				`user_email` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
    				`id_stripe` BIGINT NULL DEFAULT NULL , 
    				`deal_id` BIGINT NULL DEFAULT NULL , 
    				`user_status` BOOLEAN NOT NULL DEFAULT FALSE , 
    				`status_sync` BOOLEAN NOT NULL DEFAULT FALSE , 
    				PRIMARY KEY (`id`)
    				) ENGINE = InnoDB;";

			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			dbDelta( $sql );

			add_option( 'pipedrive_table_created', true );
		}
	}

	/**
	 * Create new trial lead send in pipedrive.
	 *
	 * @param WPCF7_ContactForm $form Contact from 7.
	 *
	 * @return void
	 * @throws APIException
	 */
	public function create_new_trial_user( WPCF7_ContactForm $form ) {
		if ( 14 === $form->id() ) {

			$email = ! empty( $_POST['email-463'] ) ? filter_var( wp_unslash( $_POST['email-463'] ), FILTER_SANITIZE_EMAIL ) : null;

			if ( $email ) {
				$pipedrive = new PipedriveApi();
				$pipedrive->create_trial_lead( (object) [ 'email' => $email ] );
			}
		}
	}
}
