<?php
/**
 * Handler class after stripe payment.
 *
 * @package iwp/iamaster
 */

namespace AiMastery\Theme;

/**
 * StripePaid class file.
 */
class StripePaid {

	/**
	 * Inner Id session data in db.
	 *
	 * @var int
	 */
	public int $inner_id;

	/**
	 * Stripe checkout session data.
	 *
	 * @var object
	 */
	public object $checkout_session;

	/**
	 * Stripe order id.
	 *
	 * Description: This is the id in the webhook of the checkout.session.completed stripe located in the
	 * data->object->id object
	 *
	 * @var string
	 */
	private string $order_id;

	/**
	 * StripePaid construct.
	 *
	 * @param string $order_id Stripe order id.
	 */
	public function __construct( string $order_id ) {
		$this->order_id = $order_id;
	}

	/**
	 * File read.
	 *
	 * @return $this
	 */
	public function read_file_to_json(): StripePaid {
		require_once ABSPATH . 'wp-admin/includes/file.php';
		WP_Filesystem();

		$file_path = get_stylesheet_directory() . '/stripe_order/' . $this->order_id . '.json';

		if ( file_exists( $file_path ) ) {
			global $wp_filesystem;
			$content                = json_decode( $wp_filesystem->get_contents( $file_path ) );
			$this->checkout_session = (object) $content;
		}

		return $this;
	}

	/**
	 * Set session data in db.
	 *
	 * @return $this
	 */
	public function set_session_in_db(): StripePaid {

		global $wpdb;
		$table_name = $wpdb->prefix . Main::AI_TABLE_NAME_STRIPE;

		$wpdb->insert(
			$table_name,
			[
				'stripe_event_id'  => $this->order_id,
				'amount_total'     => $this->checkout_session->data->object->amount_total,
				'currency'         => $this->checkout_session->data->object->currency,
				'customer_details' => wp_json_encode( $this->checkout_session->data->object->customer_details ),
				'payment_status'   => $this->checkout_session->data->object->payment_status,
			],
			[
				'%s',
				'%d',
				'%s',
				'%s',
				'%s',
			]
		);

		$this->inner_id = $wpdb->insert_id;

		return $this;
	}

	/**
	 * Delete temporary session file.
	 *
	 * @return bool|int
	 */
	public function delete_file_seesion() {
		$file_path = get_stylesheet_directory() . '/stripe_order/' . $this->order_id . '.json';

		if ( file_exists( $file_path ) ) {
			if ( unlink( $file_path ) ) {
				return true;
			} else {
				return false;
			}
		}

		return 0;
	}

	/**
	 * Get customer details.
	 *
	 * @return object
	 */
	public function get_customer_details(): object {

		if ( ! empty( $this->checkout_session->data ) ) {
			$data = [
				'city'         => $this->checkout_session->data->object->customer_details->address->city,
				'country'      => $this->checkout_session->data->object->customer_details->address->country,
				'line1'        => $this->checkout_session->data->object->customer_details->address->line1,
				'line2'        => $this->checkout_session->data->object->customer_details->address->line2 ?? '',
				'postal_code'  => $this->checkout_session->data->object->customer_details->address->postal_code,
				'state'        => $this->checkout_session->data->object->customer_details->address->state ?? '',
				'email'        => $this->checkout_session->data->object->customer_details->email,
				'name'         => $this->checkout_session->data->object->customer_details->name,
				'phone'        => $this->checkout_session->data->object->customer_details->phone,
				'amount_total' => $this->checkout_session->data->object->amount_total,
				'currency'     => $this->checkout_session->data->object->currency,
			];

			return (object) $data;
		}

		return $this->get_custromet_details_in_db( $this->order_id );
	}


	/**
	 * Get customer details in db.
	 *
	 * @param string $stripe_id Stripe id.
	 *
	 * @return object
	 */
	private function get_custromet_details_in_db( string $stripe_id ): object {
		global $wpdb;
		$table_name = $wpdb->prefix . Main::AI_TABLE_NAME_STRIPE;

		$result = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT * FROM $table_name WHERE stripe_event_id = %s",
				esc_attr( $stripe_id )
			),
			OBJECT
		);

		if ( empty( $result ) ) {
			return (object) [];
		}

		$details = json_decode( $result[0]->customer_details );

		$data = [
			'city'         => $details->address->city,
			'country'      => $details->address->country,
			'line1'        => $details->address->line1,
			'line2'        => $details->address->line2 ?? '',
			'postal_code'  => $details->address->postal_code,
			'state'        => $details->address->state ?? '',
			'email'        => $details->email,
			'name'         => $details->name,
			'phone'        => $details->phone,
			'amount_total' => $result[0]->amount_total,
			'currency'     => $result[0]->currency,
		];

		return (object) $data;

	}


}
