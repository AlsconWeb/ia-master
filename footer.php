<?php
/**
 * Footer theme template.
 *
 * @package iwp/iamaster
 */

?>
<footer>
	<div class="vc_row wpb_row vc_row-fluid vc_row-o-content-middle vc_row-flex">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="wpb_column vc_column_container">
						<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-flex">
							<div
								class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-6 vc_col-xs-12">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<?php the_custom_logo(); ?>
										<?php
										echo wp_kses_post( wpautop( carbon_get_theme_option( 'ai_footer_text' ) ) );

										echo do_shortcode( carbon_get_theme_option( 'ai_footer_shot_code_form' ) );
										?>
									</div>
								</div>
							</div>
							<div
								class="wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-3 vc_col-xs-12">
								<div class="vc_column-inner">
									<?php
									if ( is_active_sidebar( 'footer-one' ) ) {
										dynamic_sidebar( 'footer-one' );
									}
									?>
								</div>
							</div>
							<div
								class="wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-3 vc_col-xs-12">
								<div class="vc_column-inner">
									<?php
									if ( is_active_sidebar( 'footer-two' ) ) {
										dynamic_sidebar( 'footer-two' );
									}
									?>
								</div>
							</div>
							<div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-6 vc_col-xs-6">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<?php
										printf(
											'<p class="copyright">%s %d %s</p>',
											esc_html( __( 'Copyright', 'ai-mastery' ) ),
											esc_html( gmdate( 'Y' ) ),
											esc_html( __( 'AI Mastery Academy', 'ai-mastery' ) )
										);
										?>
									</div>
								</div>
							</div>
							<div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-6 vc_col-xs-6">
								<div class="vc_column-inner">
									<div class="wpb_wrapper">
										<?php
										if ( has_nav_menu( 'footer_menu' ) ) {
											wp_nav_menu(
												[
													'theme_location' => 'footer_menu',
													'container'      => '',
													'menu_class'     => '',
													'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
												]
											);
										}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<div class="modal fade modal-form">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h2
				class="after"
				data-text="<?php echo esc_attr( carbon_get_theme_option( 'ai_modal_text' ) ); ?>">
				<?php echo wp_kses_post( carbon_get_theme_option( 'ai_modal_title' ) ); ?>
			</h2>
			<?php echo do_shortcode( '[contact-form-7 id="14" title="form"]' ); ?>
		</div>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>
