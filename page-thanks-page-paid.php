<?php
/**
 * Template Name: stripe thanks page.
 *
 * @package iwp/iamaster
 */

use AiMastery\Theme\Apis\Pipedrive\PipedriveApi;
use AiMastery\Theme\StripePaid;

get_header();

if ( ! empty( $_GET['id'] ) ) {
	$stripe_id = filter_var( wp_unslash( $_GET['id'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS );

	$payment = new StripePaid( $stripe_id );

	$payment->read_file_to_json();
	if ( ! empty( $payment->checkout_session ) ) {
		$payment->set_session_in_db()->delete_file_seesion();
	}

	$customer_details = $payment->get_customer_details();

	if ( ! empty( $customer_details->name ) ) {
		$pipedrive = new PipedriveApi();

		$pipedrive->create_lead( $payment->get_customer_details() );
	}
}
?>
	<section class="content-wrapper">
		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();

				the_content();
			}
		}
		?>
	</section>
<?php
get_footer();
