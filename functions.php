<?php
/**
 * Theme functions file.
 *
 * @package iwp/iamaster
 */

use AiMastery\Theme\Main;

require_once __DIR__ . '/vendor/autoload.php';

define( 'AI_THEME_ASSETS_URL', get_stylesheet_directory_uri() . '/assets' );

new Main();
