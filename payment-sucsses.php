<?php
/**
 * Payment successes create file.
 *
 * @package iwp/iamaster
 */

const AI_ORDER_FOLDER_PATH = __DIR__ . '/stripe_order';

$pay     = @file_get_contents( 'php://input' );
$payload = json_decode( $pay );

create_temporary_order_file( $payload );

/**
 * Create temporary order file.
 *
 * @param object $payload Object payload from stripe.
 *
 * @return void
 */
function create_temporary_order_file( object $payload ) {

	if ( empty( $payload ) ) {
		return;
	}

	if ( ! file_exists( AI_ORDER_FOLDER_PATH ) ) {
		mkdir( AI_ORDER_FOLDER_PATH, 0755, true );
	}

	$file = fopen( AI_ORDER_FOLDER_PATH . '/' . $payload->data->object->id . '.json', 'a+' );

	if ( $file ) {
		fwrite( $file, json_encode( $payload ) );
		fclose( $file );
	}

	http_response_code( 200 );
}
